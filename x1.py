import os

b = 5
x = 4

# def x1():
#     print("Hello")

# def x2():
#     print(b) # b from globle

# def x3():
#     b=4
#     print(b) # b from function

# def x4(b):
#     print(b) # b from variable

# def x5():
#     x=10
#     print(x+b) # b from globle

# def x6(b):
#     x=5
#     print(x*b) # b from variable

def x7():
    x=10
    return x

def ping(x):
    ping = "ping {}".format(x)
    # print(p)
    x = os.system(ping)
    return x


def main():
    # x1()
    # x2()
    # x3()
    # x4(10)
    # x5()
    # x6(7)
    b = x7()
    print(b)
    c = "1.1.1.1"
    # g = "8.8.8.8"
    # i = "9.9.9.9"
    f = "192.168.0.255"
    x = ping(f)
    print(x)
    # ping(g)
    # ping(i)
main()
